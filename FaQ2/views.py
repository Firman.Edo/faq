from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Ask

def index1(request):
    return render(request, 'ques_answ.html')

def index2(request):
    return render(request, 'index.html')

def addQuestion(request):
    Parameter = False
    nama = ""
    question = Ask.objects.all()

    if request.method == 'POST':
        Parameter = True
        name = request.POST.get("nama")
        pertanyaan = request.POST.get("qoa")
        q = Ask.objects.create(nama = name, qoa = pertanyaan)
        q.save()
        nama = name

    context = {
        'param' : Parameter,
        'answer' : question
    }
    return render(request, 'ques_answ.html', context)

        
