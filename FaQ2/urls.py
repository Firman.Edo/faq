from django.urls import path
from .views import index1, index2, addQuestion
from . import views
app_name = "FaQ2"


urlpatterns = [
    path('', views.addQuestion, name='add_question'),
    path('FaQ/', views.index2, name='index2'),
    path('tanyajawab', views.index1, name='index1'),
]