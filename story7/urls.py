from django.urls import path
from .views import story7
from . import views
app_name = "story7"

urlpatterns = [
    path('', views.story7, name='story7'),
]