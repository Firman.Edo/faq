from django.urls import path
from . import views
from .views import cari, data

app_name = "story8"

urlpatterns = [
    path('', views.cari, name='story8'),
    path('data/', views.data, name='data'),
]