from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests
import json


def cari(request):
    response = {}
    return render(request, 'story8.html', response)

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)

    data = json.loads(ret.content)
    # data = {}
    return JsonResponse(data, safe=False)
